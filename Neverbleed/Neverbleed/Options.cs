﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Neverbleed
{
    public partial class Options : Form
    {
        
        private int index = -1;
        private List<Filter> filters = new List<Filter>( );

        public Options( List<Filter> filters )
        {
            InitializeComponent( );
            this.filters = filters;

            foreach( var filter in filters )
            {
                ListViewItem item = new ListViewItem( filter.filterString );
                item.BackColor = filter.bkColor;
                item.ForeColor = filter.fgColor;
                item.Checked = filter.enabled;
                listView1.Items.Add( item );
            }
            listView1.Columns[ 0 ].Width = listView1.Width - 5;

            this.FormClosing += new FormClosingEventHandler( Options_FormClosing );            
            this.Resize += new EventHandler( Options_Resize );

        }

        void Options_Resize( object sender, EventArgs e )
        {
            listView1.Columns[ 0 ].Width = listView1.Width - 5;
        }

        void Options_FormClosing( object sender, FormClosingEventArgs e )
        {
            filters.Clear( );
            foreach( ListViewItem item in listView1.Items )
            {
                Filter filter = new Filter( );
                filter.fgColor = item.ForeColor;
                filter.bkColor = item.BackColor;
                filter.filterString = item.Text;
                filter.enabled = item.Checked;
                filters.Add( filter );

                Console.WriteLine( filter.bkColorAsArgb );
                Console.WriteLine( filter.fgColorAsArgb );
            }
        }

        
       
        private void button1_Click( object sender, EventArgs e )
        {
            if( index > 0 )
            {
                listView1.Items.RemoveAt( index );
                index -= 1;
            }
        }

        private void addButtonaddButton_Click( object sender, EventArgs e )
        {
            ListViewItem item = new ListViewItem( textBox1.Text );
            item.ForeColor = Color.White;
            item.BackColor = Color.Black;
            item.Checked = true;
            listView1.Items.Add( item );
           // listView1.AutoResizeColumn( 0, ColumnHeaderAutoResizeStyle.ColumnContent );
        }


        private void changeButton_Click( object sender, EventArgs e )
        {
            if( index > 0 )
            {
                ListViewItem item = listView1.FocusedItem;
                item.Text = textBox1.Text;                
            }
        }

        private void upButton_Click( object sender, EventArgs e )
        {
            if( index > 1 )
            {
                ListViewItem item = listView1.Items[ index - 1 ];

                int oldIndex = index;

                listView1.Items.RemoveAt( index - 1 );
                listView1.Items.Insert( index, item );

                index = oldIndex - 1;

                listView1.FocusedItem = listView1.Items[ index ];

                if( index == 1 )
                    upButton.Enabled = false;

                downButton.Enabled = true;
            }
        }

        private void downButton_Click( object sender, EventArgs e )
        {
            if( index > 0 && index < listView1.Items.Count )
            {
                ListViewItem item = listView1.Items[ index ];
                int oldIndex = index;

                listView1.Items.RemoveAt( index );
                listView1.Items.Insert( oldIndex + 1, item );
                
                index = oldIndex + 1;
                
                listView1.FocusedItem = listView1.Items[ index ];

                if( index == listView1.Items.Count - 1 )
                    downButton.Enabled = false;

                 upButton.Enabled = true;
            }
        }



        private void listView1_SelectedIndexChanged( object sender, EventArgs e )
        {
            if( listView1.FocusedItem != null && listView1.FocusedItem.Index >= 0 && listView1.FocusedItem.Selected )
            {

                index = listView1.FocusedItem.Index;

                if( index == 0 )
                {
                    upButton.Enabled = false;
                    downButton.Enabled = false;

                    removeButton.Enabled = false;
                    changeButton.Enabled = false;
                }
                else
                {
                    if( index == 1 )
                        upButton.Enabled = false;
                    else
                        upButton.Enabled = true;

                    if( index == listView1.Items.Count - 1 )
                        downButton.Enabled = false;
                    else
                        downButton.Enabled = true;

                    removeButton.Enabled = true;
                    changeButton.Enabled = true;
                }

                bcButton.Enabled = true;
                fcButton.Enabled = true;

                this.textBox1.Text = listView1.Items[ index ].Text;
            }
            else
            {
                index = -1;
                changeButton.Enabled = false;
                removeButton.Enabled = false;

                upButton.Enabled = false;
                downButton.Enabled = false;

                bcButton.Enabled = false;
                fcButton.Enabled = false;
            }
        }

        private void fcButton_Click( object sender, EventArgs e )
        {
            colorDialog1.Color = listView1.FocusedItem.ForeColor;
            if( colorDialog1.ShowDialog( ) == DialogResult.OK )
            {
                listView1.FocusedItem.ForeColor = colorDialog1.Color;
            }
        }

        private void bcButton_Click( object sender, EventArgs e )
        {
            colorDialog1.Color = listView1.FocusedItem.BackColor;
            if( colorDialog1.ShowDialog( ) == DialogResult.OK )
            {
                listView1.FocusedItem.BackColor = colorDialog1.Color;
            }
        }
    }
}

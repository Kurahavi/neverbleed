﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

namespace Neverbleed
{
    public partial class Form1 : Form
    {
        static readonly IntPtr HWND_TOPMOST = new IntPtr( -1 );
        static readonly IntPtr HWND_NOTOPMOST = new IntPtr( -2 );
        static readonly IntPtr HWND_TOP = new IntPtr( 0 );
        static readonly IntPtr HWND_BOTTOM = new IntPtr( 1 );
        const UInt32 SWP_NOSIZE = 0x0001;
        const UInt32 SWP_NOMOVE = 0x0002;
        const UInt32 TOPMOST_FLAGS = SWP_NOMOVE | SWP_NOSIZE;
        
        
        [DllImport( "user32.dll" )]
        [return: MarshalAs( UnmanagedType.Bool )]
        public static extern bool SetWindowPos( IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags );


        private const string strFormat = "{0:0.00}";


        private int mouseDownX = 0;
        private int mouseDownY = 0;
        private string logFileName = "";

        Parser parser = null;
        List<Filter> filters = new List<Filter>();

        XmlSerializer xmlSer = new XmlSerializer( typeof( List<Filter> ) );

        private string filterFileName = "neverbleedfilter.xml";
        private string filterFullName = "";

        XmlSerializer configSerializer = new XmlSerializer( typeof( NeverBleedConfig ) );
        private string configFileName = "neverbleedconfig.xml";
        NeverBleedConfig config = new NeverBleedConfig( );

        public Form1( )
        {
            InitializeComponent( );

            getFilterPath( );
            deserializeConfig( );

            this.TopMost = true;

            this.ControlBox = false;
            this.Text = string.Empty;
            this.ShowInTaskbar = true;

            this.listView1.MouseDown += new MouseEventHandler( Form1_MouseDown );
            this.listView1.MouseMove += new MouseEventHandler( Form1_MouseMove );
            this.FormClosing += new FormClosingEventHandler( Form1_FormClosing );

            logFileName = config.LogFilePath;
            parser = new Parser( logFileName, this, filters, config.interval );

            if( logFileName == "" || logFileName == string.Empty )
                openFilePicker( );

            for( int i = 0; i < listView1.Columns.Count; ++i )
            {
                if( config.ColumnWidths.Count <= i )
                    listView1.Columns[ i ].Width = 100;
                else
                    listView1.Columns[ i ].Width = config.ColumnWidths[ i ];

                if( config.ColumnOrder.Count <= i )
                    listView1.Columns[ i ].DisplayIndex = i;
                else
                    listView1.Columns[ i ].DisplayIndex = config.ColumnOrder[ i ];
            }

            this.Width = config.Size.X;
            this.Height = config.Size.Y;

            this.StartPosition = FormStartPosition.Manual;
            this.Location = config.Location;

            this.fontDialog1.Font = listView1.Font = config.ParserFont;
        }

        private void deserializeConfig( )
        {
            FileInfo info = new FileInfo( configFileName );
            configFileName = info.FullName;

            if( File.Exists( configFileName ) )
            {
                TextReader textReader = new StreamReader( info.FullName );
                try
                {
                    config = ( NeverBleedConfig )configSerializer.Deserialize( textReader );
                    textReader.Close( );
                }
                catch( Exception )
                {
                    config = new NeverBleedConfig( );
                    textReader.Close( );   
                }

            }

        }
        private void serializeConfig( )
        {
            TextWriter t = new StreamWriter( configFileName );
            configSerializer.Serialize( t, config );
            t.Close( );
        }

        private void getFilterPath( )
        {
            if( !File.Exists( filterFileName ) )
            {
                //File.Create( filterFileName );
                FileInfo tmpInfo = new FileInfo( filterFileName );
                
                Filter filter = new Filter( );
                filter.filterString = "Everyone";
                filters.Add( filter );
                
                TextWriter t = new StreamWriter( tmpInfo.FullName );
                xmlSer.Serialize( t, filters );
                t.Close( );
                
            }
            FileInfo info = new FileInfo( filterFileName );

            TextReader textReader = new StreamReader(info.FullName);
            filters = (List<Filter>)xmlSer.Deserialize(textReader);
            textReader.Close();

            

            Console.WriteLine( info.FullName );
            filterFullName = info.FullName;
        }


        
        List<int> parseString( string data )
        {
            List<int> ints = new List<int>( );
            string[ ] strings = data.Split( ',' );

            foreach( string s in strings )
            {
                int result = 0;
                int.TryParse( s, out result );
                ints.Add( result );
            }
            return ints;
        }
        

        void Form1_FormClosing( object sender, FormClosingEventArgs e )
        {
            config.ColumnOrder.Clear( );
            config.ColumnWidths.Clear( );

            foreach( ColumnHeader column in listView1.Columns )
            {
                config.ColumnOrder.Add( column.DisplayIndex );
                config.ColumnWidths.Add( column.Width );
            }

            config.Size = new Point( this.Width, this.Height );
            config.Location = new Point( Location.X, Location.Y );
            config.LogFilePath = logFileName;
            config.ParserFont = fontDialog1.Font;

            if( parser != null )
                parser.stop( );



            serializeConfig( );
        }

        public void updateView( )
        {
            if( parser == null )
                return;

            listView1.Invoke( new Action( ( ) => listView1.Items.Clear() ) );

            // listView1.Items.Clear( );
            for( int i = 0; i < parser.targets.Count; ++i ) 
            {
                Player player = parser.targets[ i ];
                if( !player.show )
                    continue;

                ListViewItem item = new ListViewItem(
                    new string[ ] 
                    { 
                        player.ticksLeft.ToString(),
                        player.stacks.ToString(),
                        player.flags,
                        player.ticks.ToString(),
                        //player.Value.dt.ToLongTimeString(), 
                        player.name, 
                        //player.Value.id, 
                        player.trueDmg.ToString(),
                        String.Format( strFormat, ((1.0 - ( player.damage / player.trueDmg ) )*100.0) ) + "%", 
                        player.damage.ToString()
                    } );
                item.BackColor = player.bkCol;
                item.ForeColor = player.fgCol;
                listView1.Invoke( new Action( ( ) => listView1.Items.Add( item ) ) );
                //listView1.Items.Add( item );
            }
        }

        void Form1_MouseMove( object sender, MouseEventArgs e )
        {
            if( e.Button == MouseButtons.Left )
            {
                int dX = e.X - mouseDownX;
                int dY = e.Y - mouseDownY;

                this.Location = new Point( Location.X + dX, Location.Y + dY );
            }
        }
        bool chooseFile( )
        {
            openFileDialog1.Filter = "Log Files (*.log)|*.log";
            openFileDialog1.Title = "Log file";

            if( openFileDialog1.ShowDialog( ) == DialogResult.Cancel )
                return false;

            try
            {
                logFileName = openFileDialog1.FileName;
            }

            catch( Exception )
            {
                MessageBox.Show( "Error opening file", "File Error",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation );

                return false;
            }
            return true;
        }

        void Form1_MouseDown( object sender, MouseEventArgs e )
        {
            if( e.Button == MouseButtons.Left )
            {
                mouseDownX = e.X;
                mouseDownY = e.Y;
            }
//            if( e.Button == MouseButtons.Right )
//                this.Close( );
        }

        private void listView1_SelectedIndexChanged( object sender, EventArgs e )
        {

        }

        private void Form1_Load_1( object sender, EventArgs e )
        {
            SetWindowPos( this.Handle, HWND_TOPMOST, 0, 0, 0, 0, TOPMOST_FLAGS );
        }

        private void showClericToolStripMenuItem_Click( object sender, EventArgs e )
        {
            if( parser != null )
                parser.setCleric( showClericToolStripMenuItem.Checked );
        }

        private void exitToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.Close( );
        }

        private void optionsToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Thread t = new Thread( ( ) =>
            {
                Options options = new Options( filters );
                options.ShowDialog( );
            } );
            t.SetApartmentState( ApartmentState.STA );
            t.Start( );
            t.Join( );

            TextWriter tw = new StreamWriter( this.filterFullName );
            xmlSer.Serialize( tw, filters );
            tw.Close( );
        }

        private void selectLogFileToolStripMenuItem_Click( object sender, EventArgs e )
        {
            openFilePicker( );
        }


        private void openFilePicker( )
        {
            Thread t = new Thread( ( ) =>
            {
                if( chooseFile( ) )
                    parser.setFilename( logFileName );

            } );
            t.SetApartmentState( ApartmentState.STA );
            t.Start( );
            t.Join( );
        }

        private void selectFontToolStripMenuItem_Click( object sender, EventArgs e )
        {
            Thread t = new Thread( ( ) =>
            {
                fontDialog1.ShowDialog( );
            } );
            t.SetApartmentState( ApartmentState.STA );
            t.Start( );
            t.Join( );



            listView1.Font = fontDialog1.Font;
        }

        private void setIntervalForCheckingLogToolStripMenuItem_Click( object sender, EventArgs e )
        {
            IntervalDialog dialog = new IntervalDialog( config.interval );
            Thread t = new Thread( ( ) =>
            {
                dialog.ShowDialog( );
            } );
            t.SetApartmentState( ApartmentState.STA );
            t.Start( );
            t.Join( );

            if( dialog.wasOk )
            {
                config.interval = dialog.interval;
                parser.changeInterval( config.interval );
            }
        }
    }
}

﻿namespace Neverbleed
{
    partial class Options
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose( bool disposing )
        {
            if( disposing && ( components != null ) )
            {
                components.Dispose( );
            }
            base.Dispose( disposing );
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent( )
        {
            this.addButtonaddButton = new System.Windows.Forms.Button( );
            this.removeButton = new System.Windows.Forms.Button( );
            this.upButton = new System.Windows.Forms.Button( );
            this.downButton = new System.Windows.Forms.Button( );
            this.bcButton = new System.Windows.Forms.Button( );
            this.fcButton = new System.Windows.Forms.Button( );
            this.textBox1 = new System.Windows.Forms.TextBox( );
            this.changeButton = new System.Windows.Forms.Button( );
            this.listView1 = new System.Windows.Forms.ListView( );
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader( );
            this.colorDialog1 = new System.Windows.Forms.ColorDialog( );
            this.SuspendLayout( );
            // 
            // addButtonaddButton
            // 
            this.addButtonaddButton.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.addButtonaddButton.Location = new System.Drawing.Point( 12, 247 );
            this.addButtonaddButton.Name = "addButtonaddButton";
            this.addButtonaddButton.Size = new System.Drawing.Size( 75, 23 );
            this.addButtonaddButton.TabIndex = 1;
            this.addButtonaddButton.Text = "Add";
            this.addButtonaddButton.UseVisualStyleBackColor = true;
            this.addButtonaddButton.Click += new System.EventHandler( this.addButtonaddButton_Click );
            // 
            // removeButton
            // 
            this.removeButton.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.removeButton.Enabled = false;
            this.removeButton.Location = new System.Drawing.Point( 175, 247 );
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size( 75, 23 );
            this.removeButton.TabIndex = 2;
            this.removeButton.Text = "Remove";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler( this.button1_Click );
            // 
            // upButton
            // 
            this.upButton.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.upButton.Enabled = false;
            this.upButton.Location = new System.Drawing.Point( 256, 12 );
            this.upButton.Name = "upButton";
            this.upButton.Size = new System.Drawing.Size( 24, 23 );
            this.upButton.TabIndex = 3;
            this.upButton.Text = "↑";
            this.upButton.UseVisualStyleBackColor = true;
            this.upButton.Click += new System.EventHandler( this.upButton_Click );
            // 
            // downButton
            // 
            this.downButton.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.downButton.Enabled = false;
            this.downButton.Location = new System.Drawing.Point( 256, 41 );
            this.downButton.Name = "downButton";
            this.downButton.Size = new System.Drawing.Size( 24, 23 );
            this.downButton.TabIndex = 4;
            this.downButton.Text = "↓";
            this.downButton.UseVisualStyleBackColor = true;
            this.downButton.Click += new System.EventHandler( this.downButton_Click );
            // 
            // bcButton
            // 
            this.bcButton.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.bcButton.Enabled = false;
            this.bcButton.Location = new System.Drawing.Point( 256, 70 );
            this.bcButton.Name = "bcButton";
            this.bcButton.Size = new System.Drawing.Size( 24, 23 );
            this.bcButton.TabIndex = 5;
            this.bcButton.Text = "B";
            this.bcButton.UseVisualStyleBackColor = true;
            this.bcButton.Click += new System.EventHandler( this.bcButton_Click );
            // 
            // fcButton
            // 
            this.fcButton.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.fcButton.Enabled = false;
            this.fcButton.Location = new System.Drawing.Point( 256, 99 );
            this.fcButton.Name = "fcButton";
            this.fcButton.Size = new System.Drawing.Size( 24, 23 );
            this.fcButton.TabIndex = 6;
            this.fcButton.Text = "T";
            this.fcButton.UseVisualStyleBackColor = true;
            this.fcButton.Click += new System.EventHandler( this.fcButton_Click );
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.textBox1.BackColor = System.Drawing.Color.Black;
            this.textBox1.ForeColor = System.Drawing.Color.White;
            this.textBox1.Location = new System.Drawing.Point( 13, 221 );
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size( 237, 20 );
            this.textBox1.TabIndex = 7;
            // 
            // changeButton
            // 
            this.changeButton.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left ) ) );
            this.changeButton.Enabled = false;
            this.changeButton.Location = new System.Drawing.Point( 94, 247 );
            this.changeButton.Name = "changeButton";
            this.changeButton.Size = new System.Drawing.Size( 75, 23 );
            this.changeButton.TabIndex = 8;
            this.changeButton.Text = "Change";
            this.changeButton.UseVisualStyleBackColor = true;
            this.changeButton.Click += new System.EventHandler( this.changeButton_Click );
            // 
            // listView1
            // 
            this.listView1.Anchor = ( ( System.Windows.Forms.AnchorStyles )( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
                        | System.Windows.Forms.AnchorStyles.Left )
                        | System.Windows.Forms.AnchorStyles.Right ) ) );
            this.listView1.BackColor = System.Drawing.Color.Black;
            this.listView1.CheckBoxes = true;
            this.listView1.Columns.AddRange( new System.Windows.Forms.ColumnHeader[ ] {
            this.columnHeader1} );
            this.listView1.ForeColor = System.Drawing.Color.White;
            this.listView1.FullRowSelect = true;
            this.listView1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point( 13, 12 );
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size( 237, 203 );
            this.listView1.TabIndex = 9;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler( this.listView1_SelectedIndexChanged );
            // 
            // colorDialog1
            // 
            this.colorDialog1.AnyColor = true;
            this.colorDialog1.Color = System.Drawing.Color.White;
            // 
            // Options
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size( 292, 273 );
            this.Controls.Add( this.listView1 );
            this.Controls.Add( this.changeButton );
            this.Controls.Add( this.textBox1 );
            this.Controls.Add( this.fcButton );
            this.Controls.Add( this.bcButton );
            this.Controls.Add( this.downButton );
            this.Controls.Add( this.upButton );
            this.Controls.Add( this.removeButton );
            this.Controls.Add( this.addButtonaddButton );
            this.Name = "Options";
            this.Text = "Options";
            this.ResumeLayout( false );
            this.PerformLayout( );

        }

        #endregion

        private System.Windows.Forms.Button addButtonaddButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.Button upButton;
        private System.Windows.Forms.Button downButton;
        private System.Windows.Forms.Button bcButton;
        private System.Windows.Forms.Button fcButton;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button changeButton;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColorDialog colorDialog1;
    }
}
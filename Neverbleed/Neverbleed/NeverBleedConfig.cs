﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using System.Xml.Serialization;
using System.ComponentModel;

namespace Neverbleed
{
    public class NeverBleedConfig
    {
        private static TypeConverter converter = TypeDescriptor.GetConverter( typeof( Font ) );

        public List<int> ColumnOrder = new List<int>( );
        public string LogFilePath = "";
        public Point Location = new Point( 100, 100 );
        public Point Size = new Point( 200, 300 );
        public List<int> ColumnWidths = new List<int>( );
        public int interval = 400;

        [XmlIgnore]
        public Font ParserFont = new Font( FontFamily.GenericSansSerif, 8.25f );

        [XmlElement( "Font" )]
        public string FontString
        {
            get
            {
                if( ParserFont != null )
                {
                    return converter.ConvertToString( this.ParserFont );
                }
                else return null;
            }
            set
            {                               
                this.ParserFont = (Font)converter.ConvertFromString( value );
            }
        }
    }
}

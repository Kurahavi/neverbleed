﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Neverbleed
{
    public class Parser
    {
        string[ ] rogueSkills = new string[ ] { "Duelist's Flurry Bleed", "Duelist's Flurry Hit" };
        string rogueDamageType = "Physical";
        int rogueTicks = 10;
        string flurryHitCrit = "";

        string[ ] clericSkills = new string[ ] { "Brand of the Sun" };
        string clericDamageType = "Radiant";
        int clericTicks = 6;

        string[ ] checkSkills;
        string checkDamageType;
        int checkTicks = 10;

        private long dataPoint = 0;
        private string fileName = "";
        private const string dateFormat = "yy:MM:dd:HH:mm:ss.f";
        private Form1 form;

        public List<Player> targets = new List<Player>( );
        public List<String> players = new List<string>( );
        private static System.Timers.Timer timer = new System.Timers.Timer( 1000 ); // 1s

        private List<int> columnSizes = new List<int>( );
        private List<Filter> filters = new List<Filter>( );

        public Parser( string fileName, Form1 form, List<Filter> filters, int interval )
        {
            this.form = form;
            this.fileName = fileName;

            checkSkills = rogueSkills;
            checkDamageType = rogueDamageType;

            timer.Interval = interval;
            timer.Enabled = true;

            timer.Elapsed += new System.Timers.ElapsedEventHandler( timer_Elapsed );

            this.filters = filters;
        }
        public void changeInterval( int interval )
        {
            timer.Interval = interval;
        }
        public void setFilename( string fileName )
        {
            this.fileName = fileName;
        }
        void timer_Elapsed( object sender, System.Timers.ElapsedEventArgs e )
        {                        
            if( fileName != "" )
                readFile( fileName );
        }

        void fswatcher_Changed( object sender, FileSystemEventArgs e )
        {
            if( e.FullPath == fileName )
            {
                Thread.Sleep( 1000 );
                readFile( e.FullPath );
            }
        }


        public void readFile( string fileName )
        {

            //if file exists
            if( File.Exists( fileName ) )
            {
                Stream stream = File.Open( fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite );

                if( dataPoint == 0 || dataPoint > stream.Length )
                {
                    stream.Seek( 0, SeekOrigin.End );
                }
                else
                {
                    stream.Seek( dataPoint, SeekOrigin.Begin );
                }

                StreamReader streamReader = new StreamReader( stream );
                string allLines = streamReader.ReadToEnd( );
                dataPoint = stream.Position;
                streamReader.Close( );
                stream.Close( );

                parseString( allLines );

                List<int> toDelete = new List<int>( );
                for( int i = 0; i < targets.Count; ++i ) 
                {
                    if( targets[ i ].dt < DateTime.Now.Subtract( new TimeSpan( 0, 0, 2 ) ) )
                    {                        
                        toDelete.Insert( 0, i );
                    }
                }
                for( int i = 0; i < toDelete.Count; ++i )
                {
                    targets.RemoveAt( toDelete[ i ] );
                }
                form.updateView( );
            }
        }
        public void setCleric( bool showCleric )
        {
            if( showCleric )
            {
                this.checkDamageType = clericDamageType;
                this.checkSkills = clericSkills;
                this.checkTicks = clericTicks;
            }
            else
            {
                this.checkSkills = rogueSkills;
                this.checkDamageType = rogueDamageType;
                this.checkTicks = rogueTicks;
            }
        }

        public void parseString( string data )
        {
            using( StringReader reader = new StringReader( data ) )
            {
                string line;
                while( ( line = reader.ReadLine( ) ) != null )
                {
                    string[ ] argsAll = line.Split( new string[ ] { "::" }, StringSplitOptions.None );
                    string[ ] args = argsAll[ 1 ].Split( ',' );

                    bool found = false;

                    foreach( var skillName in checkSkills )
                        if( skillName == args[ args.Length - 6 ] ) found = true;



                    if( !found ) continue;

                    if( args[ args.Length - 4 ] != checkDamageType ) continue;



                    DateTime dt = DateTime.ParseExact( argsAll[ 0 ], dateFormat, null, DateTimeStyles.None );

                    Player target = new Player( );

                    parsePlayers( ref dt, ref target, ref args );

                    found = false;
                    for( int index = filters.Count - 1; index > 0; --index )
                    {
                        if( filters[index].enabled && line.Contains( filters[ index ].filterString ) )
                        {
                            target.show = true;
                            target.bkCol = filters[ index ].bkColor;
                            target.fgCol = filters[ index ].fgColor;
                            found = true;

                            break;
                        }
                    }
                    if( !found )
                    {
                        if( filters.Count >= 1 && filters[ 0 ].enabled )
                        {
                            target.show = true;
                            target.bkCol = filters[ 0 ].bkColor;
                            target.fgCol = filters[ 0 ].fgColor;
                        }
                    }
                }
            }
        }

        private void parsePlayers( ref DateTime dt, ref Player target, ref string[ ] args )
        {
            int matches = 0;
            string name = "";
            string matchString = "";
            string skillsName = args[ args.Length - 6 ];

            double newDmg = double.Parse( args[ args.Length - 2 ], CultureInfo.InvariantCulture );
            double trueDmg = double.Parse( args[ args.Length - 1 ], CultureInfo.InvariantCulture );
            if( trueDmg == 0.0 ) trueDmg = newDmg;


            for( int i = args.Length - 7; i >= 0; --i )
            {
                string dataThing = args[ i ];
                Match match = Regex.Match( dataThing, "((P|C)+\\[.*?\\])" );

                if( dataThing.Length > 0 && match.Length == dataThing.Length )
                {
                    if( matches == 0 )
                    {
                        matchString = dataThing;
                        target.id = matchString;

                        int index = targets.FindIndex( item => item.id == dataThing );
                        if( index >= 0 )
                        {
                            target = targets[ index ];
                        }
                        else // adding new one
                        {
                            this.targets.Add( target );
                            target.ticks = 0;
                            target.stacks = 0;
                        }
                         string tmpStr = args[ args.Length - 3 ];

                        if( skillsName == checkSkills[ 0 ] )
                        {
                            target.dt = dt;
                            ++target.ticks;
                            if( target.trueDmg != trueDmg )
                            {
                                target.damage = newDmg;
                                target.trueDmg = trueDmg;
                                target.id = matchString;
                                target.ticksLeft = checkTicks;
                                ++target.stacks;

                               

                                if( tmpStr.Contains( "Critical" ) && tmpStr.Contains( "Flank" ) ) target.flags = "CF";
                                else if( args[ args.Length - 3 ].Contains( "Flank" ) ) target.flags = "F";
                                else if( args[ args.Length - 3 ].Contains( "Critical" ) ) target.flags = "C";
                                else target.flags = "";

                                //if( flurryHitCrit == "CC" ) target.flags = "C" + target.flags;
                            
                            }
                            else
                            {
                                target.damage = newDmg;
                                --target.ticksLeft;
                            }
                        }
                        else if( checkSkills.Count( ) > 1 && skillsName == checkSkills[ 1 ] )
                        {
                            target.ticksLeft = checkTicks;
                            if( tmpStr.Contains( "Critical" ) && flurryHitCrit.Equals( "C" )) flurryHitCrit = "CC";  
                            else if( tmpStr.Contains( "Critical" ) ) flurryHitCrit = "C";
                            else flurryHitCrit = "";
                        }
                    }
                    else if ( matches == 1 )
                    {
                        target.name = name;
                        break;
                    }

                    ++matches;
                    name = "";
                }
                else
                    name += dataThing;
            }

        }
        public void stop( )
        {
            timer.Stop( );
        }

    }
}



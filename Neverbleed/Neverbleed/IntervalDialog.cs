﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Neverbleed
{
    public partial class IntervalDialog : Form
    {
        public bool wasOk = false;
        public int interval = 400;

        public IntervalDialog( int interval )
        {
            InitializeComponent( );

            this.textBox1.Text = interval.ToString( );
        }

        private void button1_Click( object sender, EventArgs e )
        {
            try
            {
                interval = int.Parse( textBox1.Text );
            }
            catch( Exception ) 
            {
                interval = 400;
            }
            if( interval < 400 ) interval = 400;
            wasOk = true;
            this.Close( );
        }

        private void button2_Click( object sender, EventArgs e )
        {
            wasOk = false;
            this.Close( );
        }
    }
}

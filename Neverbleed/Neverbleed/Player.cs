﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace Neverbleed
{
    public class Player
    {
        public DateTime dt = DateTime.Now;
        public string name = "";
        public string id = "";
        public double damage = 0.0;
        public double trueDmg = 0.0;
        public int ticks = 0;
        public int ticksLeft = 0;
        public int position = 0;

        public int stacks = 0;
        public string flags = "";

        public bool over = false;

        public bool show = false;

        public Color bkCol = Color.Black;
        public Color fgCol = Color.White;
    }
}

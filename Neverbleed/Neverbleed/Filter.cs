﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Xml.Serialization;



namespace Neverbleed
{
    public class Filter
    {
        public string filterString = "";
        public bool enabled = true;


        [XmlIgnore( )]
        public Color bkColor = Color.Black;
        [XmlIgnore( )]
        public Color fgColor = Color.White;

        [XmlElement( "bgColor" )]
        public int bkColorAsArgb
        {
            get { return bkColor.ToArgb( ); }
            set { bkColor = Color.FromArgb( value ); }
        }

        [XmlElement( "fgColor" )]
        public int fgColorAsArgb
        {
            get { return fgColor.ToArgb( ); }
            set { fgColor = Color.FromArgb( value ); }
        }
    }
}
